#!/usr/bin/env python

from opoge.msgpackrpc.client import SyncClient
from opoge.msgpackrpc import errors

client = SyncClient(('127.0.0.1', 10000))
assert client.request('ping') == 'pong'
try:
    client.request('does_not_exist')
except errors.MethodNotFound:
    pass # this is expected
else:
    assert False, 'Expected MethodNotFound'
