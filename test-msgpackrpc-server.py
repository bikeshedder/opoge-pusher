#!/usr/bin/env python

import logging
logging.basicConfig(level='DEBUG')

from opoge.msgpackrpc.server import MessagePackRpcStreamServer
from opoge.msgpackrpc import errors

class Server(MessagePackRpcStreamServer):
    def handle_call(self, method, params):
        if method == 'ping':
            return 'pong'
        raise errors.MethodNotFound()

server = Server(('', 10000))
server.serve_forever()
