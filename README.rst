============
Opoge Pusher
============

The opoge pusher implements a very simple publish subscribe protocol
that can be used for browser push.


Sessions and topics
===================

A session is basicly a queue where browsers can pull messages
from. The pulling is implemented as long pollig so browsers
hang until a message is ready for them or timeout (default: 30s)
occurs. In the case of a timeout an empty message list is
returned and the browser is expected to reconnect.

.. note::
    Session and object ids should only contain letters, decimal
    digits, and hyphens.

Since the session id is the only mean of authenticating users
when pulling messages it is a good idea to use a rather long
and random session id. A good choice for this is a UUID.

The choice of topic ids are completely up to the application
developer.

.. tip::
    It is a good idea to use topic identifiers that includes
    the namespace and type of the topic.
    
    A good example for a topic id is "thread.12345"
    which could be used for watching the thread with id 12345
    in a web based forum.

.. tip::
    Object ids can be rather long without compromising
    performance since hash tables are used for looking them up.

.. important::
    Topic ids starting with "opoge.pusher." are reserved for
    pusher internal topics. Currently only "opoge.pusher.debug"
    is implemented which provides session and object events.


Pulling messages
================

The following commands are accessable by anyone and do not
require authorization.


Pull messages
-------------

URL::

    POST /

Headers::

    X-Session-ID: <session-id>

Returns::

    {
        'seq': <sequence-number>,
        'messages': [
            <message[0]>,
            <message[1]>,
            ...
        ]
    }

Message format::

    [
        <topic-id>,
        <message-data>
    ]

.. note::
    The sequence number is a plain integer that starts at 0 for the
    first pull call and is incremented every time a pull request is
    performed. This gives the client the ability to check if messages
    were missed when retrying a pull operation because of a connection
    timeout.

.. note::
    Only one client is ever allowed to pull data for the same session.
    If a client is currently waiting for a pull request and another
    client requests to pull for the same session the second client
    will receive an error.


Control API
===========

The following commands utilize MessagePack RPC and a normal
TCP socket. There are two categories of control commands:

   1. session control
   2. topic control


Create session
--------------

Create a new session.

This method does nothing if the session does already
exist.

Method::

    session.create

Parameters::

    session_id

Returns::

    null


Delete session
--------------

Delete existing session with given id.

This method does nothing if no session with the given
session id exists.

Method::

    session.delete

Parameters::

    session_id

Returns::

    null


Add subscription
----------------

Subscribe session to a topic. If the topic does not exist
it is created first.

Method::

    session.subscribe

Parameters::

    session_id
    topic_id

Returns::

    null


Delete subscription
-------------------

Unsubscribe session from a topic.

Method::

    session.unsubscribe

Parameters::

    session_id
    topic_id

Returns::

    null


Get list of sessions
--------------------

Method::

    session.list

Parameters::

    (none)

Returns::

   [
        {
            "id": <session_id>,
            "active": <boolean>,
            "lastActivity": <int>,
            "topicCount": <int>,
            "messageCount": <int>
        },
        ...
    ]


Get session details
-------------------

This method returns some session details and a list of all
subscriptions.

Method::

    session.get

Parameters::

    session_id

Returns::

    {
        "id": <session_id>,
        "active": <boolean>,
        "lastActivity": <int>,
        "topics": [
            <oid>,
            ...
        ],
        "messages": [
            <message[0]>,
            <message[1]>,
            ...
        ]
    }


Push message to session
-----------------------

Push message to a single session.

Method::

    session.push

Parameters::

    session_id
    message

Returns::

    null


Get list of topics
------------------

Method::

    topic.list

Parameters::

    (none)

Returns::

    [
        {
            "id": <topic_id>,
            "sessionCount": <int>
        },
        ...
    ]


Get topic details
-----------------

Get topic details. This method returns the object and the list
of all sessions.

Method::

    topic.get

Parameters::

    topic_id

Returns::

    {
        "id": <topic_id>,
        "sessions": [
            <session_id>,
            ...
        ]
    }

.. note::
    Topics are create implicitly when creating subscriptions for
    them. Therefore the only way to check if an topic exists is
    to check the length of the sessions attribute.


Push message to object
----------------------

Push message to a topic. This method looks up the subscriptions
for the topic and dispatches it to the sessions.

Method::

    topic.push

Parameters::

    topic_id
    message_data

Returns::

    null


Interactive debug page
======================

The interactive debug page can be accessed via `/debug/` and is
accessable to all configured users in the configuration file.


Introspection
=============

By subscribing the object "opoge.pusher.debug" information about
sessions and objects can be retrieved. All debug messages are of
the form::

    [<action-name>, <data>]

The following session actions are available:

    - session.create
    - session.delete
    - session.update

These provide data of the form::

    {
        "id": <session_id>,
        "active": <boolean>,
        "lastActivity": <int>,
        "topicCount": <int>,
        "messageCount": <int>
    }

The following topic actions are available:

    - topic.create
    - topic.delete
    - topic.update

These provide data of the form::

    {
        "id": <topic_id>,
        "sessionCount": <int>
    }

.. note::
    The \*.delete actions only provide the id of the session or
    topic and not the entire data dictionary.
