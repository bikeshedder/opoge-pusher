try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(name='opoge-pusher',
        version='0.1.0',
        description='HTTP push server with a topic-based publish/subscribe protocol',
        author='Michael P. Jung',
        author_email='michael.jung@terreon.de',
        url='http://opoge.org/pusher/',
        packages=['opoge', 'opoge.msgpackrpc', 'opoge.pusher'],
        package_data={
            'opoge.pusher': [
                'templates/*.html',
                'static/*.js'
            ]
        },
        scripts=['bin/opoge-pusher'])
