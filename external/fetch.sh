#!/bin/bash

set -e

cd $(dirname $0)
EXTERNAL_DIR=$PWD

function check_hash() {
	filename=$1
	sha1=$2
	if [[ $sha1 != "" ]] ; then
		echo "$sha1  $filename" | sha1sum --check -
		return $?
	fi
}

function download() {
	url=$1
	filename=$2
	sha1=$3
	download=true
	if [ -e $filename ] ; then
		check_hash $filename $sha1 && download=false
	fi
	if [ $download == true ] ; then
		curl --location $url > $filename
		if [ $? != 0 ] ; then
			echo "Download failed: $url"
			exit 1
		fi
		check_hash $filename $sha1
	fi
}

#
# Clean directories
#

mkdir -p download

rm -rf extract python javascript
mkdir extract python javascript

#
# gevent 0.13.6
#
download \
	http://pypi.python.org/packages/source/g/gevent/gevent-0.13.6.tar.gz \
	download/gevent-0.13.6.tar.gz \
	097e59b4586ce6bf099d5eae2962b830fda073eb
(cd extract; tar -xzf ../download/gevent-0.13.6.tar.gz)
(cd extract/gevent-0.13.6; python setup.py build)
(cd python; ln -s ../extract/gevent-0.13.6/build/lib*/gevent .)

#
# greenlet 0.3.1
#
download \
	http://pypi.python.org/packages/source/g/greenlet/greenlet-0.3.1.tar.gz \
	download/greenlet-0.3.1.tar.gz \
	7e42d736dfbbd2f0b5ab7f78dc6fb65d966ca21e
(cd extract; tar -xzf ../download/greenlet-0.3.1.tar.gz)
(cd extract/greenlet-0.3.1; python setup.py build)
(cd python; ln -s ../extract/greenlet-0.3.1/build/lib*/greenlet.so .)

#
# msgpack-python 0.1.10
#
download \
	http://pypi.python.org/packages/source/m/msgpack-python/msgpack-python-0.1.10.tar.gz \
	download/msgpack-python-0.1.10.tar.gz \
	099f48d5debb89e6811f7570b39954f512169866
(cd extract; tar xvzf ../download/msgpack-python-0.1.10.tar.gz)
(cd extract/msgpack-python-0.1.10; python setup.py build)
(cd python; ln -s ../extract/msgpack-python-0.1.10/build/lib.*/msgpack .)

#
# jQuery 1.6.4
#
download \
	http://code.jquery.com/jquery-1.6.4.min.js \
	download/jquery-1.6.4.min.js \
	71cce71820cc47b3bd1098618d248325fcf24ddb
(cd javascript; ln -s ../download/jquery-1.6.4.min.js .)

download \
	http://code.jquery.com/jquery-1.6.4.js \
	download/jquery-1.6.4.js \
	921e7702ac9e4c4a4bca052b7bc83b0304440ee3
(cd javascript; ln -s ../download/jquery-1.6.4.js .)

#
# Knockout 1.3.0beta
#
download \
	http://cloud.github.com/downloads/SteveSanderson/knockout/knockout-1.3.0beta.js \
	download/knockout-1.3.0beta.min.js \
	c65349d464e41c05ce6b2426c48e7bd075384867
(cd javascript; ln -s ../download/knockout-1.3.0beta.min.js .)

download \
	http://cloud.github.com/downloads/SteveSanderson/knockout/knockout-1.3.0beta.debug.js \
	download/knockout-1.3.0beta.js \
	ba7e4c93cb20d043217a3f7c4cda05888fd71f28
(cd javascript; ln -s ../download/knockout-1.3.0beta.js .)
