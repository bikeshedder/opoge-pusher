#!/usr/bin/env python

import os
import sys
import time

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

sys.path[1:1] = [
    os.path.join(PROJECT_ROOT, 'external', 'python'),
]

from opoge.pusher.client import ControlClient
client = ControlClient(('127.0.0.1', 6327), 'admin', 'admin')
client.client.request('session.create', 'foo-session')
client.client.request('topic.create', 'bar-topic')
client.client.request('session.subscribe', 'foo-session', 'bar-topic')
client.client.request('topic.push', 'bar-topic', 'hello')
