#!/usr/bin/env python

import os
import sys

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

sys.path[1:1] = [
    os.path.join(PROJECT_ROOT, 'external', 'python'),
]

import opoge.pusher.server
opoge.pusher.server.main()
