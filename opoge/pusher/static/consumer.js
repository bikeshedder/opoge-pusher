(function() {

	// Create opoge.pusher namespace if it does not exist
	if (typeof this.opoge === 'undefined') {
		this.opoge = {};
	}
	if (typeof this.opoge.pusher === 'undefined') {
		this.opoge.pusher = {};
	}
	var ns = this.opoge.pusher;

	var scriptUrl = $('script').last().attr('src');

	function domainFromUrl(url) {
		var m = url.match(/^https?:\/\/([^:\/]+)/);
		if (m === null) {
			// Assume an URL without domain part - this is only
			// useful when the debug page references the consumer.js
			return document.domain;
		}
		return m[1];
	}

	var tunnelDomain = domainFromUrl(scriptUrl);

	function commonDomain(a, b) {
		var a = a.split('.').reverse();
		var b = b.split('.').reverse();
		var c = [];
		for (var i=0; i<Math.min(a.length, b.length); ++i) {
			if (a[i] !== b[i]) {
				break;
			}
			c.push(a[i]);
		}
		return c.reverse().join('.');
	}

	var corsDomain = commonDomain(document.domain, tunnelDomain);

	try {
		document.domain = corsDomain;
	} catch(e) {
		alert(e);
	}

	// Autodetect URL for tunnel iframe and pass corsDomain
	var tunnelUrl = scriptUrl.match('^(.*)/consumer\.js$')[1] +
			'/tunnel/#' + corsDomain

	/**
	 * opoge.pusher.Consumer
	 */
	var Consumer = ns.Consumer = function(options) {
		if (this === ns || this === window) {
			throw new Error('opoge.pusher.Consumer must be called as a constructor');
		}
		if ($('#opoge-pusher-tunnel').length > 0) {
			throw new Error('Only one instance of opoge.pusher.Consumer must ever be created');
		}
		this.sid = options.sid;
		this.running = false;
		this.jqXHR = null;
		this.subscribers = [];
		// create tunnel iframe
		this.tunnel = document.createElement('iframe');
		var self = this;
		$(this.tunnel)
			.attr('id', 'opoge-pusher-tunnel')
			.attr('src', tunnelUrl)
			.appendTo('body')
			.load(function() {
				self.$ = self.tunnel.contentWindow.$;
			});
	};
	Consumer.prototype.ready = function(callback) {
		var self = this;
		$(this.tunnel).load(function() {
			callback.apply(self);
		});
	}
	Consumer.prototype.start = function() {
		if (this.sid === null) {
			throw new Error('Call setup() first');
		}
		if (this.running) {
			throw new Error('Already running');
		}
		this._pull();
		this.running = true;
	};
	Consumer.prototype.stop = function() {
		if (this.jqXHR !== null) {
			this.jqXHR.abort();
			this.jqXHR = null;
		}
		this.running = false;
	};
	Consumer.prototype.subscribe = function(topicRegExp, callback) {
		var subscriber = new Subscriber(this, topicRegExp, callback);
		this.subscribers.push(subscriber);
	};
	Consumer.prototype.unsubscribe = function(subscription) {
		for (i=0; i<this.subscribers; ++i) {
			if (this.subscribers[i] == subscription) {
				this.subscribers.splice(i, 1);
				return;
			}
		}
	};
	Consumer.prototype.processMessage = function(message) {
		var topic = message[0];
		var data = message[1];
		// XXX Should we really mangle the topic to a string just
		//     for matching with regular expressions?
		var topicString = topic.toString();
		for (var i=0; i<this.subscribers.length; ++i) {
			var subscriber = this.subscribers[i];
			if (topicString.match(subscriber.topicRegExp)) {
				subscriber.callback(topic, data);
			}
		}
	};
	Consumer.prototype._pull = function() {
		if (typeof this.sid === 'undefined') {
			throw new Error('sid is mandatory');
		}
		if (typeof this.$ === 'undefined') {
			throw new Error('Consumer is not ready, yet');
		}
		var self = this;
		this.jqXHR = this.$.ajax({
			type: 'POST',
			url: '/',
			headers: {
				'X-Session-Id': this.sid
			},
			success: function(data) {
				var messages = data.messages;
				for (var i=0; i<messages.length; ++i) {
					self.processMessage(messages[i]);
				}
				self._pull();
			}
		});
	}

	function Subscriber(consumer, topicRegExp, callback) {
		this.consumer = consumer;
		this.topicRegExp = topicRegExp;
		this.callback = callback;
	}
	Subscriber.prototype.unsubscribe = function() {
		this.consumer.unsubscribe(this);
	};

}());
