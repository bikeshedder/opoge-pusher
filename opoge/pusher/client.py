#!/usr/bin/env python2.7

import itertools
import socket
import sys

import json
import msgpack

from opoge.msgpackrpc.client import SyncClient
import opoge.pusher.errors


class ControlClient(object):

    def __init__(self, address, username, password):
        self.address = address
        self.username = username
        self.password = password
        self.msgid_gen = itertools.count()
        self.connect()
        self.login()

    def connect(self):
        self.client = SyncClient(self.address)

    def login(self):
        self.client.request('login', self.username, self.password)

    def push(self, topic_id, message):
        return client.request('topic.push', topic_id, message)

    def get_topic(self, topic_id):
        return Topic(self.client, topic_id)

    def get_session(self, session_id):
        return Session(self.client, session_id)


class Topic(object):

    def __init__(self, client, topic_id):
        self.client = client
        self.topic_id = topic_id

    def push(self, message):
        self.client.request('topic.push', self.topic_id, message)


class Session(object):

    def __init__(self, client, session_id):
        self.client = client
        self.session_id = session_id

    def push(self, message):
        self.client.request('session.push', self.session_id, message)
