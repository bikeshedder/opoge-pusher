from opoge.msgpackrpc.errors import RpcError


class Unauthorized(RpcError):
    code = 401
    message = 'Unauthorized'
