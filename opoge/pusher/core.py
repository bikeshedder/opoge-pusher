import logging
from time import time

from gevent.queue import Queue
from gevent.queue import Empty as QueueEmpty


logger = logging.getLogger(__name__)


class Realm(object):
    '''A realm is a collection of topics and sessions. Topics and
    sessions can not span more than one realm.'''

    def __init__(self, id):
        self.id = id
        self.topics = {}
        self.sessions = {}
        self.debug_topic = DebugTopic(self)

    def check_login(self, username, password):
        # XXX implement
        return True

    def get_topic(self, id, create=False):
        '''Get session by id. If create is True a new topic is
        created if it does not exist otherwise a Topic.DoesNotExist
        exception is raised.'''
        try:
            return self.topics[id]
        except KeyError:
            if create:
                return Topic(self, id)
            else:
                raise Topic.DoesNotExist

    def get_session(self, id, create=False):
        '''Get session by id. If create is True a new session is
        created if it does not exist otherwise a Session.DoesNotExist
        exception is raised.'''
        try:
            return self.sessions[id]
        except KeyError:
            if create:
                return Session(self, id)
            else:
                raise Session.DoesNotExist

    def gc_sessions(self, timeout):
        '''Garbage collect sessions. This removes all dead sessions.
        A session is considered dead if it has been inactive for a given
        time amount.

        Keyword arguments:
        timeout -- seconds until a session is considered dead
        '''
        alive_time = time() - timeout
        active_count = 0
        alive_count = 0
        dead_sessions = []
        for session in self.sessions.itervalues():
            if session.active:
                active_count += 1
            elif session.last_activity >= alive_time:
                alive_count += 1
            else:
                dead_sessions.append(session)
        for session in dead_sessions:
            session.delete()
        logger.debug('GC: sessions active/alive/dead: %d/%d/%d' %
                (active_count, alive_count, len(dead_sessions)))

    def gc_topics(self):
        '''Garbage collect topics. This removes the topic from the
        topics dictionary if there are no sessions left and the
        topic is not marked as persistent.'''
        used_count = 0
        unused_topics = []
        for topic in self.topics.itervalues():
            if topic.sessions or topic.persistent:
                used_count += 1
            else:
                unused_topics.append(topic)
        logger.debug('GC: topics used/unused: %d/%d' %
                (used_count, len(unused_topics)))
        for topic in unused_topics:
            topic.delete()
            logger.debug('GC: unused topic %r deleted' % topic.id)


class Topic(object):

    def __init__(self, realm, id, persistent=False):
        self.id = id
        self.realm = realm
        self.sessions = set()
        self.persistent = persistent
        self.realm.topics[id] = self
        if not isinstance(self, DebugTopic):
            # Only one debug topic is supposed to exist and while
            # creating it we can not push any debug messages to it.
            self.realm.debug_topic.push(
                    ('topic.create', self.get_overview_dict()))

    def __hash__(self):
        return hash(self.id)

    def __str__(self):
        return 'Topic(%r)' % self.id

    def get_overview_dict(self):
        return {
            'id': self.id,
            'persistent': self.persistent,
            'sessionCount': len(self.sessions)
        }

    def delete(self):
        for session in self.sessions:
            session.unsubscribe(self)
        self.sessions.clear()
        self.realm.topics.pop(self.id, None)
        self.realm.debug_topic.push(('topic.delete', {'id': self.id}))

    def push(self, message):
        for session in self.sessions:
            session.push(self, message)

    class DoesNotExist(RuntimeError):
        pass


class DebugTopic(Topic):
    def __init__(self, realm):
        super(DebugTopic, self).__init__(realm, 'opoge.pusher.debug', persistent=True)


class Session(object):

    def __init__(self, realm, id, debug=False):
        self.realm = realm
        self.id = id
        self.active = False
        self.last_activity = time()
        self.topics = set()
        self.messages = Queue()
        self.debug = debug
        self.realm.sessions[id] = self
        if self.debug:
            self.realm.debug_topic.push(
                    ('session.create', {'id': self.id}))
        else:
            self.realm.debug_topic.push(
                    ('session.create', self.get_overview_dict()))

    def __hash__(self):
        return hash(self.id)

    def __str__(self):
        return 'Session(%r)' % self.id

    def subscribe(self, topic):
        self.topics.add(topic)
        topic.sessions.add(self)
        self.push_debug_update()
        self.realm.debug_topic.push(
                ('topic.update', topic.get_overview_dict()))

    def unsubscribe(self, topic):
        self.topics.discard(topic)
        topic.sessions.discard(self)
        self.push_debug_update()
        self.realm.debug_topic.push(
                ('topic.update', topic.get_overview_dict()))

    def push_debug_update(self):
        # Only push update for non-debug sessions. Otherwise we would
        # end up in an infinite loop when pushing messages to a debug
        # session.
        if not self.debug:
            self.realm.debug_topic.push(
                    ('session.update', self.get_overview_dict()))

    def push(self, topic, message):
        self.messages.put((topic, message))
        self.push_debug_update()

    def pull(self, timeout=None):
        self.active = True
        self.push_debug_update()
        try:
            # fetch first message from queue and block
            message = self.messages.get(timeout=timeout)
            if message is StopIteration:
                return
            yield message
            # fetch all remaining messages from queue
            while True:
                message = self.messages.get_nowait()
                if message is StopIteration:
                    return
                yield message
        except QueueEmpty:
            return
        finally:
            self.last_activity = time()
            self.active = False
            self.push_debug_update()

    def delete(self):
        self.messages.put(StopIteration)
        for topic in list(self.topics):
            self.unsubscribe(topic)
        self.realm.sessions.pop(self.id, None)
        self.realm.debug_topic.push(('session.delete', {'id': self.id}))

    def get_overview_dict(self):
        if self.debug:
            return {
                'id': self.id,
                'topicCount': len(self.topics),
            }
        else:
            return {
                'id': self.id,
                'active': self.active,
                'lastActivity': int(self.last_activity),
                'topicCount': len(self.topics),
                'messageCount': self.messages.qsize(),
            }

    class DoesNotExist(RuntimeError):
        pass
