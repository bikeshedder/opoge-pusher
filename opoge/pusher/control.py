from opoge.msgpackrpc.errors import MethodNotFound
from opoge.pusher.core import Session
from opoge.pusher.core import Topic
from opoge.pusher.errors import Unauthorized


class ControlApi(object):

    def __init__(self, realm):
        self.realm = realm
        self.authorized = False

    def __call__(self, method_name, params):
        method = self.get_method(method_name)
        return method(*params)

    def get_method(self, method_name):
        if '__' in method_name:
            raise MethodNotFound()
        method_name = method_name.replace('.', '__')
        try:
            return getattr(self, 'method__%s' % method_name)
        except AttributeError:
            raise MethodNotFound()

    def _check_auth(self):
        if not self.authorized:
            raise Unauthorized()

    def method__login(self, username, password):
        if self.realm.check_login(username, password):
            self.authorized = True
            return True
        else:
            return False

    def method__session__create(self, session_id):
        self._check_auth()
        self.realm.get_session(session_id, create=True)

    def method__session__delete(self, session_id):
        self._check_auth()
        try:
            session = self.realm.get_session(session_id)
        except Session.DoesNotExist:
            return
        session.delete()

    def method__session__push(self, session_id, message):
        self._check_auth()
        try:
            session = self.realm.get_session(session_id)
        except Session.DoesNotExist:
            return
        session.push(None, message)

    def method__session__subscribe(self, session_id, topic_id):
        self._check_auth()
        try:
            session = self.realm.get_session(session_id)
        except Session.DoesNotExist:
            return
        topic = self.realm.get_topic(topic_id, create=True)
        session.subscribe(topic)

    def method__session__unsubscribe(self, session_id, topic_id):
        self._check_auth()
        try:
            session = self.realm.get_session()
        except Session.DoesNotExist:
            return
        try:
            topic = self.realm.get_topic()
        except Topic.DoesNotExist:
            return
        session.subscribe(topic)

    def method__topic__create(self, topic_id):
        self._check_auth()
        self.realm.get_topic(topic_id, create=True)

    def method__topic__delete(self, topic_id):
        self._check_auth()
        try:
            self.realm.get_topic(topic_id)
        except Topic.DoesNotExist:
            return

    def method__topic__push(self, topic_id, message):
        self._check_auth()
        try:
            topic = self.realm.get_topic(topic_id)
        except Topic.DoesNotExist:
            return
        topic.push(message)


class ControlApiFactory(object):

    def __init__(self, realm):
        self.realm = realm

    def __call__(self, socket, address):
        return ControlApi(self.realm)
