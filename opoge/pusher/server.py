from __future__ import print_function
from __future__ import absolute_import

import argparse
from ConfigParser import SafeConfigParser
import logging
import os

from gevent import sleep
from gevent import spawn

from opoge.msgpackrpc.server import MessagePackRpcStreamServer
from opoge.pusher.core import Realm
from opoge.pusher.control import ControlApiFactory
from opoge.pusher.http import PullServer
from opoge.pusher.config import Config


VACUUM_INTERVAL = 10
SESSION_TIMEOUT = 30


logger = logging.getLogger(__name__)


def vacuum_daemon(realm):
    while True:
        sleep(VACUUM_INTERVAL)
        realm.gc_sessions(SESSION_TIMEOUT)
        realm.gc_topics()


def main():

    parser = argparse.ArgumentParser(description='Opoge Pusher Server')
    parser.add_argument('-c', '--config', type=file, metavar='CFG',
            required=True, dest='config_file', help='configuration file')

    args = parser.parse_args()

    config = Config(args.config_file)

    # Initialize logging subsystem
    logging.basicConfig(level=config.logging_level)
    # Create realm
    realm = Realm('default')
    # Initialize control server
    control_api = ControlApiFactory(realm)
    control_server = MessagePackRpcStreamServer(
            config.control_address, control_api)
    # Initialize pull server
    pull_server = PullServer(
            config.pull_address, config, realm)
    try:
        pull_server.start()
        control_server.start()
        spawn(vacuum_daemon, realm).join()
    except KeyboardInterrupt:
        logger.info('Keyboard interrupt caught. Terminating.')
