from ConfigParser import SafeConfigParser

class Config(object):

    def __init__(self, fh):
        # load configuration based on environment variable
        parser = SafeConfigParser()
        parser.readfp(fh)
        self.logging_level = parser.get('logging', 'level')
        self.control_address = (
                parser.get('control', 'host'),
                parser.getint('control', 'port'))
        self.pull_address = (
                parser.get('http', 'host'),
                parser.getint('http', 'port'))
        self.cors_domain = parser.get('cors', 'domain')
        self.static_files = {
            'jquery_js': parser.get('static', 'jquery_js'),
            'jquery_ui_js': parser.get('static', 'jquery_ui_js'),
            'jquery_ui_css': parser.get('static', 'jquery_ui_css'),
            'jquery_tmpl_js': parser.get('static', 'jquery_tmpl_js'),
            'json2_js': parser.get('static', 'json2_js'),
            'knockout_js': parser.get('static', 'knockout_js'),
        }
