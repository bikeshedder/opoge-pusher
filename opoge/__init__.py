# Extend search path for the modules which compromise the opoge package.
from pkgutil import extend_path
__path__ = extend_path(__path__, __name__)
