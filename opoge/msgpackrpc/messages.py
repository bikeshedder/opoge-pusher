class Request(object):
    type = 0

    def __init__(self, msgid, method, params=None):
        if not isinstance(msgid, (int, long)):
            raise TypeError('"msgid" must be an integer')
        if not isinstance(method, basestring):
            raise TypeError('"method" must be an integer')
        if params is not None and not isinstance(params, (tuple, list)):
            raise TypeError('"params" must be a list')
        self.msgid = msgid
        self.method = method
        self.params = params or []

    @staticmethod
    def from_list(data):
        if len(data) < 3:
            raise TypeError('Too few list elements for request')
        if len(data) > 4:
            raise TypeError('Too many list elements for request')
        return Request(*data)

    def to_list(self):
        return [
            Request.type,
            self.msgid,
            self.method,
            self.params
        ]



class Response(object):
    type = 1

    def __init__(self, msgid, error=None, result=None):
        self.msgid = msgid
        self.error = error
        self.result = result

    @staticmethod
    def from_list(data):
        if len(data) < 2:
            raise TypeError('Too few list elements for response')
        if len(data) > 4:
            raise TypeError('Too many list elements for response')
        return Response(*data)

    def to_list(self):
        return [
            Response.type,
            self.msgid,
            self.error and self.error.to_list(),
            self.result
        ]


class Notification(object):
    type = 2

    def __init__(self, method, params=None):
        if not method(method, basestring):
            raise TypeError('"method" must be an integer')
        if params is not None and not isinstance(params, (tuple, list)):
            raise TypeError('"params" must be a list')
        self.method = method
        self.params = params or []

    @staticmethod
    def from_list(data):
        if len(data) < 2:
            raise TypeError('Too few list elements for notification')
        if len(data) > 3:
            raise TypeError('Too many list elements for notification')
        return Notification(*data)


def parse_message(data):
    if not isinstance(data, (tuple, list)):
        raise TypeError('Expected message encoded as list')
    msgtype = data[0]
    if msgtype == Request.type:
        message = Request.from_list(data[1:])
    elif msgtype == Response.type:
        message = Response.from_list(data[1:])
    elif msgtype == Notification.type:
        message = Notification.from_list(data[1:])
    else:
        raise TypeError('Unsupported message type')
    return message
