class_registry = {}


class RpcError(RuntimeError):
    code = 0
    message = ''
    data = None

    # Metaclass for registering subclasses of RpcError
    class __metaclass__(type):
        def __init__(cls, name, bases, dict):
            type.__init__(cls, name, bases, dict)
            class_registry.setdefault(cls.code, cls)

    def __init__(self, code=None, message=None, data=None):
        if code is not None:
            self.code = code
        if message is not None:
            self.message = message
        if data is not None:
            self.data = data

    @staticmethod
    def from_list(data):
        if len(data) < 2:
            raise TypeError('Too few elements in list')
        if len(data) > 3:
            raise TypeError('Too many elements in list')
        code = data[0]
        cls = class_registry.get(code, RpcError)
        return cls(*data)


    def to_list(self):
        l = [self.code, self.message]
        if self.data is not None:
            l.append(self.data)
        return l


class ParseError(RpcError):
    code = -32700
    message = 'Parse error'


class InvalidRequest(RpcError):
    code = -32600
    message = 'Invalid Request'


class MethodNotFound(RpcError):
    code = -32601
    message = 'Method not found'


class InvalidParams(RpcError):
    code = -32602
    message = 'Invalid params'


class InternalError(RpcError):
    code = -32603
    message = 'Internal error'
