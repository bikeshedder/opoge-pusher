from __future__ import absolute_import

import logging
logger = logging.getLogger(__name__)

from gevent.server import StreamServer
import msgpack

from opoge.msgpackrpc import errors
from opoge.msgpackrpc import messages


class MessagePackRpcStreamServer(StreamServer):

    def __init__(self, listener, api_factory):
        super(MessagePackRpcStreamServer, self).__init__(listener)
        self.api_factory = api_factory

    def handle(self, socket, address):
        api = self.api_factory(socket, address)
        logger.debug('Connection from %r', address)
        unpacker = msgpack.Unpacker()
        while True:
            data = socket.recv(2**16)
            if not data:
                logger.debug('Connection lost to %r', address)
                return
            unpacker.feed(data)
            for message_data in unpacker:
                if not self.handle_message(socket, api, message_data):
                    socket.close()

    def handle_message(self, socket, api, message_data):
        msgid = None
        keep_connection_alive = True
        try:
            try:
                message = messages.parse_message(message_data)
            except TypeError, e:
                keep_connection_alive = False
                raise errors.InvalidRequest(data=e.message)
            if isinstance(message, messages.Request):
                msgid = message.msgid
            elif not isinstance(message, messages.Notification):
                raise errors.InvalidRequest(data='Unsupported message type')
            logger.debug('Calling method %r', message.method)
            result = api(message.method, message.params)
            response = messages.Response(msgid, result=result)
            if msgid is None and response is not None:
                response = None
                logger.warning('Response for %r call ignored because client '
                        'sent a notification.', message.method)
        except errors.RpcError, e:
            logger.debug('Method call failed: %r %r', e.code, e.message)
            response = messages.Response(msgid, error=e)
        except Exception, e:
            logger.exception(e)
            response = messages.Response(msgid, error=errors.InternalError())
        if response:
            response_data = response.to_list()
            socket.send(msgpack.dumps(response_data))
        return keep_connection_alive
