import itertools
import socket
import sys

import msgpack

from opoge.msgpackrpc import messages
from opoge.msgpackrpc import errors


class SyncClient(object):

    def __init__(self, address):
        self.address = address
        self.msgid_gen = itertools.count()
        self.unpacker = msgpack.Unpacker()
        self.socket = None

    def connect(self):
        if self.socket:
            raise RuntimeError('Already connected')
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect(self.address)

    def request(self, method, *args):
        if not self.socket:
            self.connect()
        message = messages.Request(self.msgid_gen.next(), method, args)
        self.socket.send(msgpack.dumps(message.to_list()))
        while True:
            data = self.socket.recv(65535)
            if not data:
                raise RuntimeError('Connection terminated unexpectedly')
            self.unpacker.feed(data)
            for response_data in self.unpacker:
                response = messages.parse_message(response_data)
                if response.error:
                    raise errors.RpcError.from_list(response.error)
                return response.result
